from time import time as current_time

import numpy as np
import scipy.ndimage as nd
import networkx as nx

from timagetk.components import SpatialImage
from timagetk.plugins.resampling import isometric_resampling

from cellcomplex.property_topomesh.analysis import compute_topomesh_property, compute_topomesh_vertex_property_from_faces
from cellcomplex.property_topomesh.morphology import topomesh_binary_property_morphological_operation, topomesh_binary_property_fill_holes
from cellcomplex.property_topomesh.extraction import property_filtering_sub_topomesh, topomesh_connected_components, clean_topomesh
from cellcomplex.property_topomesh.optimization import topomesh_triangle_split, property_topomesh_vertices_deformation, property_topomesh_isotropic_remeshing

from cellcomplex.property_topomesh.utils.networkx_tools import topomesh_to_graph

from tissue_nukem_3d.nuclei_mesh_tools import nuclei_image_surface_topomesh, up_facing_surface_topomesh
from tissue_nukem_3d.signal_map import nuclei_density_function


def meristem_surface_topomesh(signal_img, seg_img=None, resampling_voxelsize=1., refined=False, signal_sigma=2., signal_threshold=1000, microscope_orientation=1):
    """

    Parameters
    ----------
    seg_img
    resampling_voxelsize
    microscope_orientation

    Returns
    -------

    """

    assert signal_img is not None or seg_img is not None

    if seg_img is not None:
        #binary_img = SpatialImage(nd.gaussian_filter(255*(seg_img>1), sigma=1./np.array(seg_img.voxelsize)).astype(np.uint8), voxelsize=seg_img.voxelsize)
        binary_img = SpatialImage((255 * (seg_img > 1)).astype(np.uint8), voxelsize=seg_img.voxelsize)
    else:
        #binary_img = nd.gaussian_filter(signal_img, sigma=signal_sigma/np.array(signal_img.voxelsize)) > 2.*signal_threshold
        #binary_img = SpatialImage(nd.gaussian_filter(255*binary_img, sigma=1./np.array(signal_img.voxelsize)).astype(np.uint8), voxelsize=signal_img.voxelsize)
        binary_img = SpatialImage((255 * (signal_img > 2.*signal_threshold)).astype(np.uint8), voxelsize=signal_img.voxelsize)

    # if resampling_voxelsize is not None:
    #     binary_img = isometric_resampling(binary_img, method=resampling_voxelsize, option='linear')
    # else:
    #     resampling_voxelsize = np.array(binary_img.voxelsize)

    topomesh = nuclei_image_surface_topomesh(binary_img,
                                             nuclei_sigma=resampling_voxelsize,
                                             density_voxelsize=resampling_voxelsize,
                                             maximal_length=10 * resampling_voxelsize,
                                             intensity_threshold=64,
                                             decimation=100,
                                             remeshing_iterations=20)

    topomesh = up_facing_surface_topomesh(topomesh, normal_method='orientation', upwards=microscope_orientation==1)

    if refined:
        topomesh = topomesh_triangle_split(topomesh)
        property_topomesh_vertices_deformation(topomesh, omega_forces={'taubin_smoothing': 0.66}, iterations=10)

        compute_topomesh_property(topomesh, 'normal', 2, normal_method='orientation')

    start_time = current_time()
    print("--> Computing mesh curvature")
    compute_topomesh_vertex_property_from_faces(topomesh, 'normal', neighborhood=3, adjacency_sigma=1.2)

    curvature_properties = ['mean_curvature', 'gaussian_curvature', 'principal_curvature_min', 'principal_curvature_max']
    compute_topomesh_property(topomesh, 'mean_curvature', 2)
    for property_name in curvature_properties:
        compute_topomesh_vertex_property_from_faces(topomesh, property_name, neighborhood=3, adjacency_sigma=1.2)
    print("<-- Computing mesh curvature [", current_time() - start_time, "s]")

    topomesh.update_wisp_property('barycenter_z', 0, dict(zip(topomesh.wisps(0), topomesh.wisp_property('barycenter', 0).values(list(topomesh.wisps(0)))[:, 2])))

    return topomesh


def curvature_meristem_extraction(topomesh, curvature_property='principal_curvature_min', curvature_threshold=-1e-3, image_center=None):
    """

    Parameters
    ----------
    topomesh
    curvature_property
    curvature_threshold
    image_center

    Returns
    -------

    """

    topomesh.update_wisp_property('meristem', 2, {f: topomesh.wisp_property(curvature_property, 2)[f] > curvature_threshold for f in topomesh.wisps(2)})
    for iteration in range(3):
        topomesh_binary_property_morphological_operation(topomesh, 'meristem', 2, 'dilation', iterations=1, contour_value=0)
        topomesh_binary_property_morphological_operation(topomesh, 'meristem', 2, 'erosion', iterations=1, contour_value=1)
    topomesh_binary_property_fill_holes(topomesh, 'meristem', 2)

    convex_topomesh = property_filtering_sub_topomesh(topomesh,'meristem',2,(1,1))
    compute_topomesh_property(convex_topomesh, 'area', 2)

    convex_topomeshes = topomesh_connected_components(convex_topomesh, degree=2)
    mesh_scores = []
    for i_mesh, mesh in enumerate(convex_topomeshes):
        mesh_area = mesh.wisp_property('area', 2).values(list(mesh.wisps(2))).sum()
        mesh_center = mesh.wisp_property('barycenter', 0).values(list(mesh.wisps(0))).mean(axis=0)

        if image_center is not None:
            mesh_scores += [np.sqrt(mesh_area) / np.linalg.norm((mesh_center - image_center)[:2])]
        else:
            mesh_scores += [np.sqrt(mesh_area)]
    meristem_topomesh = convex_topomeshes[np.argmax(mesh_scores)]
    meristem_topomesh = clean_topomesh(meristem_topomesh, clean_properties=True)

    return meristem_topomesh


def compute_meristem_surface_properties(meristem_topomesh, meristem_signal_data=None, surface_signals=['CLV3']):
    compute_topomesh_property(meristem_topomesh, 'contour', 0)
    compute_topomesh_property(meristem_topomesh, 'area', 2)
    compute_topomesh_property(meristem_topomesh, 'barycenter', 2)

    if meristem_signal_data is not None:
        compute_meristem_surface_signals_from_data(meristem_topomesh, meristem_signal_data, signal_names=surface_signals)
        if ('CLV3' in surface_signals) and ('CLV3' in meristem_signal_data.columns):
            compute_meristem_surface_cz_from_clv3(meristem_topomesh)

    meristem_topomesh.update_wisp_property('min_curvature', 0, meristem_topomesh.wisp_property('principal_curvature_min', 0))
    meristem_topomesh.update_wisp_property('max_curvature', 0, meristem_topomesh.wisp_property('principal_curvature_max', 0))

    compute_meristem_surface_rotational_symmetry(meristem_topomesh)
    compute_meristem_surface_centrality(meristem_topomesh)


def compute_meristem_surface_signals_from_data(meristem_topomesh, meristem_signal_data, signal_names=['CLV3'], cell_radius=5., density_k=0.66):
    start_time = current_time()
    print("--> Computing mesh surface signals")
    positions = dict(zip(range(len(meristem_signal_data)), meristem_signal_data[['center_' + dim for dim in 'xyz']].values))

    meristem_points = meristem_topomesh.wisp_property('barycenter', 0).values(list(meristem_topomesh.wisps(0)))
    x = meristem_points[..., 0]
    y = meristem_points[..., 1]
    z = meristem_points[..., 2]

    potential = [nuclei_density_function(dict([(p, -positions[p])]), cell_radius=cell_radius, k=density_k)(x, y, z) for p in range(len(positions))]
    potential = np.transpose(potential)

    density = np.sum(potential, axis=-1)
    membership = potential / density[..., np.newaxis]

    for signal_name in signal_names:
        if signal_name in meristem_signal_data.columns:
            print("  --> Computing {} surface signal".format(signal_name))
            signal_values = meristem_signal_data[signal_name].values
            meristem_signal = np.sum(membership * signal_values[np.newaxis, :], axis=-1)
            meristem_topomesh.update_wisp_property(signal_name, 0, dict(zip(meristem_topomesh.wisps(0), meristem_signal)))

            normalized_meristem_signal = meristem_signal / signal_values.mean()
            meristem_topomesh.update_wisp_property("normalized_" + signal_name, 0, dict(zip(meristem_topomesh.wisps(0), normalized_meristem_signal)))
        else:
            print("  --> {} signal in not defined in data!".format(signal_name))
    print("<-- Computing mesh surface signals [", current_time() - start_time, "s]")


def compute_meristem_surface_cz_from_clv3(meristem_topomesh, clv3_threshold_range=np.linspace(1.2, 1.8, 7)):
    start_time = current_time()
    print("--> Computing mesh central zone")

    meristem_central_zone = meristem_topomesh.wisp_property('normalized_CLV3', 0).values(list(meristem_topomesh.wisps(0))) > np.mean(clv3_threshold_range)
    if 'central_zone' in meristem_topomesh.wisp_property_names(0):
        del meristem_topomesh._wisp_properties[0]['central_zone']
    meristem_topomesh.update_wisp_property('central_zone', 0, dict(zip(meristem_topomesh.wisps(0),
                                                                       meristem_central_zone.astype(int))))
    topomesh_binary_property_morphological_operation(meristem_topomesh, 'central_zone', 0, method='opening', iterations=3)
    meristem_central_zone = meristem_topomesh.wisp_property('central_zone', 0).values(list(meristem_topomesh.wisps(0)))
    meristem_topomesh.update_wisp_property('central_zone', 0, dict(zip(meristem_topomesh.wisps(0),
                                                                       meristem_central_zone.astype(int))))

    centers = []
    radii = []
    for clv3_threshold in clv3_threshold_range:

        clv3_regions = topomesh_connected_components(property_filtering_sub_topomesh(meristem_topomesh, "normalized_CLV3", 0, (clv3_threshold, 1e5)))

        if len(clv3_regions) > 0:
            clv3_region = clv3_regions[0]
            clv3_face_areas = clv3_region.wisp_property('area', 2).values()
            clv3_face_centers = clv3_region.wisp_property('barycenter', 2).values()
            clv3_radius = np.sqrt(clv3_face_areas.sum() / np.pi)
            clv3_center = (clv3_face_areas[:, np.newaxis] * clv3_face_centers).sum(axis=0) / clv3_face_areas.sum()
            centers += [clv3_center]
            radii += [clv3_radius]

    clv3_center = np.mean(centers, axis=0)
    clv3_radius = np.mean(radii)

    vertex_points = meristem_topomesh.wisp_property('barycenter', 0).values(list(meristem_topomesh.wisps(0)))
    vertex_center_distances = np.linalg.norm(vertex_points - clv3_center, axis=1)
    clv3_center_vertex = np.array(list(meristem_topomesh.wisps(0)))[np.argmin(vertex_center_distances)]

    meristem_topomesh.update_wisp_property('center', 0, {v: int(v == clv3_center_vertex)
                                                         for v in meristem_topomesh.wisps(0)})

    print("<-- Computing mesh central zone [", current_time() - start_time, "s]")


def compute_meristem_surface_rotational_symmetry(meristem_topomesh, regression='quadratic'):

    start_time = current_time()
    print("--> Computing mesh rotational symmetry")

    vertex_normals = meristem_topomesh.wisp_property('normal', 0).values(list(meristem_topomesh.wisps(0)))

    vertex_points = meristem_topomesh.wisp_property('barycenter', 0).values(list(meristem_topomesh.wisps(0)))

    vertex_vectors = vertex_points[:, np.newaxis] - vertex_points[np.newaxis]

    vertex_dot_products = np.einsum("...ij,...ij->...i", vertex_vectors, vertex_normals)
    vertex_normal_projected_points = vertex_points[np.newaxis] + vertex_dot_products[:, :, np.newaxis] * vertex_normals[np.newaxis]

    vertex_radial_distances = np.linalg.norm(vertex_points[:, np.newaxis] - vertex_normal_projected_points, axis=-1)

    rotational_symmetry = {}
    for i_v, vid in enumerate(meristem_topomesh.wisps(0)):
        # for i_v, vid in enumerate([0]):

        rotated_r = vertex_radial_distances[:, i_v]
        rotated_z = vertex_dot_products[:, i_v]

        # r_weights = np.exp(-np.power(rotated_r, 2) / np.power(20, 2))
        r_weights = np.ones_like(rotated_z)

        if regression == 'quadratic':
            p_quadratic = np.polyfit(rotated_r, rotated_z, deg=2, w=r_weights)
            mse_quadratic = (r_weights * np.power(rotated_z - np.polyval(p_quadratic, rotated_r), 2)).sum() / (r_weights.sum())
            rotational_symmetry[vid] = 1/mse_quadratic
        elif regression == 'cubic':
            p_cubic = np.polyfit(rotated_r, rotated_z, deg=3, w=r_weights)
            mse_cubic = (r_weights * np.power(rotated_z - np.polyval(p_cubic, rotated_r), 2)).sum() / (r_weights.sum())
            rotational_symmetry[vid] = 1/mse_cubic

        # k = 1.
        # R = 2.
        # distances = np.abs(rotated_r[np.newaxis] - rotated_r[:,np.newaxis])
        # potential = 1./2. * (1. - np.tanh(k*(distances - R)))
        # mean_z = (potential * rotated_z).sum(axis=1) / potential.sum(axis=1)
        # mse_mean = (r_weights * np.power(rotated_z - mean_z, 2)).sum() / (r_weights.sum())
        # rotational_symmetry[vid] = 1/mse_mean

    meristem_topomesh.update_wisp_property('rotational_symmetry', 0, rotational_symmetry)

    print("<-- Computing mesh rotational symmetry [", current_time() - start_time, "s]")


def compute_meristem_surface_centrality(meristem_topomesh):

    start_time = current_time()
    print("--> Computing mesh centrality")

    vertex_contour = meristem_topomesh.wisp_property('contour', 0).values(list(meristem_topomesh.wisps(0))).astype(bool)

    G = topomesh_to_graph(meristem_topomesh)

    # centrality = nx.eigenvector_centrality(G, weight='length', max_iter=100, tol=1e-2)
    # meristem_topomesh.update_wisp_property('eigenvector_centrality',0,centrality)

    dijkstra = dict(nx.all_pairs_dijkstra(G, weight='length'))
    distance_matrix = np.array([[dijkstra[i][0][j] for j in G.nodes] for i in G.nodes])
    shortest_paths = np.array([[dijkstra[i][1][j] for j in G.nodes] for i in G.nodes])

    centrality = dict(zip(G.nodes, (G.number_of_nodes() - 1) / distance_matrix.sum(axis=1)))
    meristem_topomesh.update_wisp_property('closeness_centrality', 0, centrality)

    truncated_shortest_paths = np.array([[p[1:-1] for p in paths] for paths in shortest_paths])
    shortest_path_nodes = np.array([i for p in np.ravel(truncated_shortest_paths) for i in p])
    centrality = nd.sum(np.ones_like(shortest_path_nodes), shortest_path_nodes, index=G.nodes)
    centrality = dict(zip(G.nodes, centrality / ((G.number_of_nodes() - 1) * (G.number_of_nodes() - 2))))
    meristem_topomesh.update_wisp_property('betweenness_centrality', 0, centrality)

    contour_distances = distance_matrix[:, vertex_contour].min(axis=1)
    meristem_topomesh.update_wisp_property('contour_distance', 0, dict(zip(meristem_topomesh.wisps(0), contour_distances)))

    print("<-- Computing mesh centrality [", current_time() - start_time, "s]")