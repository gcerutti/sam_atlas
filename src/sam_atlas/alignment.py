import logging
import os
from time import time as current_time
from copy import deepcopy

import numpy as np
import scipy.ndimage as nd
from scipy.spatial import cKDTree, KDTree
from scipy.optimize import linear_sum_assignment
import pandas as pd
import matplotlib.pyplot as plt

from timagetk.algorithms.reconstruction import pts2transfo

from cellcomplex.property_topomesh.transformation import topomesh_transformation, transform_points
from cellcomplex.property_topomesh.utils.pandas_tools import topomesh_to_dataframe

from tissue_nukem_3d.signal_map import SignalMap
from tissue_nukem_3d.signal_map_visualization import plot_signal_map
from tissue_nukem_3d.utils.signal_luts import signal_colormaps, signal_ranges, signal_lut_ranges

import sam_atlas
from sam_atlas.utils import golden_angle

default_database_dirname = sam_atlas.__path__[0]+"/../../share/data/sam_db"

def icp_meristem_sequence_alignment(sample_meristem_topomesh_sequence, aligned_meristem_topomesh_sequences, primordium_range=[-2,-1,0,1,2], orientation_range=[-1,1], iterations=20, rotation_threshold=0.1, translation_threshold=0.05, matching_method='kd_tree', global_matching = False, matching_radius=20., curvature_weight=20.):

    sequence_names = list(aligned_meristem_topomesh_sequences.keys())

    sequence_meristem_points = {}
    sequence_meristem_curvature = {}
    all_curvature = []
    for sequence_name in sequence_names:
        sequence_meristem_topomesh = aligned_meristem_topomesh_sequences[sequence_name]
        sequence_df = pd.concat([topomesh_to_dataframe(sequence_meristem_topomesh[f], 0) for f in sequence_meristem_topomesh.keys()])
        sequence_meristem_points[sequence_name] = sequence_df[['center_x', 'center_y', 'center_z']].values
        sequence_meristem_curvature[sequence_name] = sequence_df['mean_curvature'].values
        all_curvature += list(sequence_meristem_curvature[sequence_name])
    all_curvature = np.array(all_curvature)
    meristem_center = np.concatenate([m for m in sequence_meristem_points.values()]).mean(axis=0)

    filenames = np.sort(list(sample_meristem_topomesh_sequence.keys()))

    sample_points = []
    sample_curvature = []
    for f in filenames:
        topomesh = sample_meristem_topomesh_sequence[f]
        sample_points += list(topomesh.wisp_property('barycenter', 0).values(list(topomesh.wisps(0))))
        sample_curvature += list(topomesh.wisp_property('mean_curvature', 0).values(list(topomesh.wisps(0))))
    sample_points = np.array(sample_points)
    sample_curvature = np.array(sample_curvature)
    center = sample_points.mean(axis=0)

    initial_meristem_sequence = {}
    for initial_rotation_primordium in primordium_range:
        for orientation in orientation_range:
            initial_transform = np.eye(4)
            initial_transform[:3, 3] = - (center - meristem_center)

            initial_rotation = np.eye(4)
            initial_rotation[0, 0] = np.cos(initial_rotation_primordium * golden_angle)
            initial_rotation[0, 1] = -np.sin(initial_rotation_primordium * golden_angle)
            initial_rotation[1, 1] = np.cos(initial_rotation_primordium * golden_angle)
            initial_rotation[1, 0] = np.sin(initial_rotation_primordium * golden_angle)

            initial_transform = np.dot(initial_rotation, initial_transform)

            initial_meristem_sequence[(initial_rotation_primordium, orientation)] = {}
            for f in filenames:
                topomesh = sample_meristem_topomesh_sequence[f]
                aligned_topomesh = topomesh_transformation(topomesh, initial_transform)
                aligned_topomesh = topomesh_transformation(aligned_topomesh, np.diag([1, orientation, 1, 1]))
                initial_meristem_sequence[(initial_rotation_primordium, orientation)][f] = aligned_topomesh

    sequence_kd_trees = {}
    for sequence_name in sequence_names:
        sequence_kd_trees[sequence_name] = cKDTree(sequence_meristem_points[sequence_name])

    aligned_meristem_sequence = {}
    rmsd_trajectories = {}
    for initial_rotation_primordium in primordium_range:
        for orientation in orientation_range:
            start_time = current_time()

            sample_points = []
            aligned_topomesh_sequence = {}
            for f in filenames:
                aligned_topomesh = initial_meristem_sequence[(initial_rotation_primordium, orientation)][f]
                aligned_topomesh_sequence[f] = aligned_topomesh
                sample_points += list(aligned_topomesh.wisp_property('barycenter', 0).values(list(aligned_topomesh.wisps(0))))
            sample_points = np.array(sample_points)

            iteration = 0
            rotation_angle = np.degrees(golden_angle)
            translation_distance = 100. * np.cos(golden_angle)
            rmsd_trajectories[(initial_rotation_primordium, orientation)] = []

            while iteration < iterations and (np.abs(rotation_angle) > rotation_threshold or translation_distance > translation_threshold):
                iteration_start_time = current_time()

                matching_distances = {}
                matching_curvature_errors = {}
                matched_meristem_points = {}
                matched_sample_points = {}

                sample_kd_tree = cKDTree(sample_points)

                if global_matching:
                    global_start_time = current_time()

                    global_meristem_points = np.concatenate([sequence_meristem_points[s] for s in sequence_names])
                    global_meristem_curvature = np.concatenate([sequence_meristem_curvature[s] for s in sequence_names])

                    if matching_method == 'kd_tree':
                        global_kd_tree = KDTree(global_meristem_points)
                        match = global_kd_tree.query(sample_points,k=1)

                        matching_sample_points = sample_points
                        matching_sample_curvature = sample_curvature

                        matched_points = global_meristem_points[match[1]]
                        matched_curvature = global_meristem_curvature[match[1]]

                    elif matching_method == 'hungarian':
                        hungarian_start_time = current_time()
                        distance_matrix = np.linalg.norm(global_meristem_points[:, np.newaxis] - sample_points[np.newaxis], axis=-1)
                        hungarian_matching = linear_sum_assignment(distance_matrix)
                        hungarian_matching = tuple([m[distance_matrix[hungarian_matching] < matching_radius] for m in hungarian_matching])

                        matching_sample_points = sample_points[hungarian_matching[1]]
                        matching_sample_curvature = sample_curvature[hungarian_matching[1]]

                        matched_points = global_meristem_points[hungarian_matching[0]]
                        matched_curvature = global_meristem_curvature[hungarian_matching[0]]

                    #valid_match = np.abs(matched_curvature - sample_curvature) < 3. * np.std(all_curvature)
                    valid_match = np.ones_like(matched_curvature).astype(bool)

                    matched_meristem_points['global'] = matched_points[valid_match]
                    matched_sample_points['global'] = matching_sample_points[valid_match]
                    matching_distances['global'] = np.linalg.norm(matched_points - matching_sample_points, axis=1)
                    matching_curvature_errors['global'] = np.abs(matched_curvature - matching_sample_curvature)

                    logging.info("    --> Matching global  [ " + str(current_time() - global_start_time) + " s ]")
                else:
                    for sequence_name in sequence_names:
                        sequence_start_time = current_time()

                        if matching_method == 'kd_tree':
                            kd_tree = sequence_kd_trees[sequence_name]

                            kd_tree_start_time = current_time()
                            # match = kd_tree.query(sample_points, k=1)

                            match = sample_kd_tree.query_ball_tree(kd_tree, matching_radius, 2, matching_radius)
                            non_empty_match = np.array([len(m)>0 for m in match])
                            matching_sample_points = sample_points[non_empty_match]
                            matching_sample_curvature = sample_curvature[non_empty_match]

                            match = np.array(match)[non_empty_match]
                            #match = np.array([m[0] for m in match])
                            match = np.array([m[np.argmin(np.linalg.norm(sequence_meristem_points[sequence_name][m] - p, axis=1))] for m,p in zip(match,matching_sample_points)])

                            matched_points = sequence_meristem_points[sequence_name][match]
                            matched_curvature = sequence_meristem_curvature[sequence_name][match]

                            logging.debug("      --> KD-Tree Match" + " [ " + str(current_time() - kd_tree_start_time) + " s ]")

                        elif matching_method == 'hungarian':

                            hungarian_start_time = current_time()
                            distance_matrix = np.linalg.norm(sequence_meristem_points[sequence_name][:, np.newaxis] - sample_points[np.newaxis], axis=-1)
                            hungarian_matching = linear_sum_assignment(distance_matrix)
                            hungarian_matching = tuple([m[distance_matrix[hungarian_matching] < matching_radius] for m in hungarian_matching])

                            matching_sample_points = sample_points[hungarian_matching[1]]
                            matching_sample_curvature = sample_curvature[hungarian_matching[1]]

                            matched_points = sequence_meristem_points[sequence_name][hungarian_matching[0]]
                            matched_curvature = sequence_meristem_curvature[sequence_name][hungarian_matching[0]]

                            logging.debug("      --> Hungarian Match" + " [ " + str(current_time() - hungarian_start_time) + " s ]")

                    # matching_distance = match[0]
                    matching_distance = np.linalg.norm(matched_points - matching_sample_points, axis=1)
                    matching_distance += curvature_weight * np.abs(matched_curvature - matching_sample_curvature) / np.std(all_curvature)

                    # valid_match = np.abs(matched_curvature - sample_curvature) < 5. * np.std(all_curvature)
                    valid_match = np.ones_like(matched_curvature).astype(bool)

                    matched_meristem_points[sequence_name] = matched_points[valid_match]
                    matched_sample_points[sequence_name] = matching_sample_points[valid_match]

                    matching_distances[sequence_name] = np.linalg.norm(matched_points - matching_sample_points, axis=1)
                    matching_curvature_errors[sequence_name] = np.abs(matched_curvature - matching_sample_curvature)
                    logging.debug("    --> Matching sequence " + sequence_name + " [ " + str(current_time() - sequence_start_time) + " s ]")

                all_sample_points = np.concatenate([s for s in matched_sample_points.values()])
                all_meristem_points = np.concatenate([m for m in matched_meristem_points.values()])

                alignment_transform = pts2transfo(all_sample_points,all_meristem_points)

                rotation_angle = (np.degrees(np.arctan2(alignment_transform[1, 0], alignment_transform[0, 0])) + 180) % 360 - 180
                translation_distance = np.linalg.norm(alignment_transform[:3, 3])

                matching_error = np.concatenate([d for d in matching_distances.values()])
                matching_error += curvature_weight * np.concatenate([c for c in matching_curvature_errors.values()]) / np.std(all_curvature)

                rmsd = np.sqrt(np.power(matching_error, 2).mean())
                rmsd_trajectories[(initial_rotation_primordium, orientation)] += [rmsd]

                iteration += 1
                logging.debug("  --> ICP Iteration "+str(iteration)+" [ "+str(current_time()-iteration_start_time)+" s ]")
                # print("Iteration", iteration, ": (r =", np.round(rotation_angle, 5),
                #       "°, t =", np.round(translation_distance, 5),
                #       "µm, d =", np.round(rmsd, 5), "µm)   [", current_time() - iteration_start_time, "s ]")

                sample_points = []
                for f in filenames:
                    aligned_topomesh = aligned_topomesh_sequence[f]
                    aligned_topomesh = topomesh_transformation(aligned_topomesh, alignment_transform)
                    sample_points += list(aligned_topomesh.wisp_property('barycenter', 0).values(list(aligned_topomesh.wisps(0))))
                    aligned_topomesh_sequence[f] = aligned_topomesh
                sample_points = np.array(sample_points)

            logging.info("--> P"+str(initial_rotation_primordium)+" ["+str(orientation)+"] : "+str(np.round(rmsd,5))+" µm   [ "+str(iteration)+" steps, "+str(current_time() - start_time)+" s ]")
            aligned_meristem_sequence[(initial_rotation_primordium, orientation)] = aligned_topomesh_sequence

    optimal_p, optimal_o = list(rmsd_trajectories.keys())[np.argmin([rmsd_trajectories[(p, o)][-1] for (p, o) in rmsd_trajectories.keys()])]
    return aligned_meristem_sequence[(optimal_p, optimal_o)]


def load_aligned_sam_database(database_dirname=None):
    
    if database_dirname is None:
        database_dirname = default_database_dirname
    
    aligned_sequence_database = {}
    for data_filename in [f for f in os.listdir(database_dirname) if os.path.splitext(f)[1]=='.csv']:
        file_data = pd.read_csv(database_dirname+"/"+data_filename)
        if 'filename' in file_data.columns:
            filename = file_data['filename'].values[0]
            sequence_name = filename[:-4]
            if not sequence_name in aligned_sequence_database.keys():
                 aligned_sequence_database[sequence_name] = {}
            aligned_sequence_database[sequence_name][filename] = file_data

    return aligned_sequence_database


def centered_meristem_curvature_map_alignment(sequence_data, aligned_sequences_database, curvature_type='mean_curvature', map_resolution=2, pz_radius = 80., meristem_orientation=None):

    filenames = np.sort(list(sequence_data.keys()))
    sequence_name = filenames[0][:-4]
    sequence_times = [int(f[-2:]) for f in filenames]

    for i_file, filename in enumerate(filenames):
        assert 'centered_x' in sequence_data[filename].columns

    curvature_maps = {}
    for reference_sequence_name in aligned_sequences_database.keys():
        for reference_filename in aligned_sequences_database[reference_sequence_name].keys():
            if int(reference_filename[-2:]) in sequence_times:
                logging.info("  --> Computing curvature map : "+reference_filename)
                reference_data = aligned_sequences_database[reference_sequence_name][reference_filename]

                reference_signal_map = SignalMap(reference_data, extent=100, position_name='aligned', resolution=map_resolution, polar=True, radius=5., density_k=0.33)
                reference_signal_map.compute_signal_map(curvature_type)
                curvature_maps[reference_filename] = reference_signal_map

    # for time in sequence_times:
    #     time_reference_filenames = [f for f in curvature_maps.keys() if int(f[-2:]) == time]
    #
    #     n_rows = int(np.ceil(np.sqrt(len(time_reference_filenames))))
    #
    #     figure = plt.figure(0)
    #     figure.clf()
    #
    #     for i_f, reference_filename in enumerate(time_reference_filenames):
    #         figure.add_subplot(n_rows, n_rows, i_f+1)
    #
    #         plot_signal_map(curvature_maps[reference_filename], curvature_type, figure,
    #                         colormap=signal_colormaps[curvature_type],
    #                         signal_range=signal_ranges[curvature_type],
    #                         signal_lut_range=signal_lut_ranges[curvature_type])
    #
    #     figure.set_size_inches(5*n_rows, 5*n_rows)
    #     figure.tight_layout()
    #     figure.savefig("SAM_database_t{}_curvature_maps.png".format(str(time).zfill(2)))

    # average_maps = {}
    # for time in range(24):
    #     time_reference_filenames = [f for f in curvature_maps.keys() if int(f[-2:]) == time]
    #
    #     if len(time_reference_filenames)>0:
    #         average_signal_map = deepcopy(curvature_maps[time_reference_filenames[0]])
    #         average_signal_map.confidence_map = np.nanmean([np.sum(curvature_maps[f].potential,axis=-1) for f in time_reference_filenames], axis=0)
    #         average_signal_map.confidence_map = nd.gaussian_filter(average_signal_map.confidence_map, sigma=1.0 / average_signal_map.resolution)
    #         for signal_name in average_signal_map.signal_maps.keys():
    #             average_signal_map.signal_maps[signal_name] = np.nansum([np.sum(curvature_maps[f].potential,axis=-1)*curvature_maps[f].signal_maps[signal_name] for f in time_reference_filenames],axis=0)
    #             average_signal_map.signal_maps[signal_name] /= np.nansum([np.sum(curvature_maps[f].potential,axis=-1) for f in time_reference_filenames],axis=0)
    #         average_maps[time] = average_signal_map
    #
    # figure = plt.figure(0)
    # figure.clf()
    #
    # figure.set_size_inches(10 * len(average_maps), 10)
    # figure.tight_layout()
    #
    # for i_t,time in enumerate(average_maps.keys()):
    #     figure.add_subplot(1,len(average_maps),i_t+1)
    #     plot_signal_map(average_maps[time], curvature_type, figure,
    #                     colormap=signal_colormaps[curvature_type],
    #                     signal_range=signal_ranges[curvature_type],
    #                     signal_lut_range=signal_lut_ranges[curvature_type])
    #     figure.gca().set_title("Aligned t" + str(time).zfill(2))
    #
    # figure.set_size_inches(10 * len(average_maps), 10)
    # figure.tight_layout()
    # figure.savefig("SAM_database_average_curvature_maps.png")

    rotation_curvature_errors = {}
    rotation_relative_curvature_errors = {}
    rotation_inverted_curvature_errors = {}
    rotation_inverted_relative_curvature_errors = {}

    rotation_overlap = {}
    rotation_inverted_overlap = {}

    for i_file, filename in enumerate(filenames):
        logging.info("  --> Evaluating rotation error : "+filename)
        file_data = sequence_data[filename]

        # map_figure = plt.figure(1)
        # map_figure.clf()

        signal_map = SignalMap(file_data, extent=100, position_name='centered', resolution=map_resolution, polar=True, radius=5., density_k=0.33)
        signal_map.compute_signal_map(curvature_type)

        time_reference_filenames = [f for f in curvature_maps.keys() if int(f[-2:]) == int(filename[-2:])]

        # map_figure.add_subplot(3,len(time_reference_filenames)+1,(len(time_reference_filenames)+1)+1)
        # plot_signal_map(signal_map,curvature_type,map_figure,
        #                 colormap=signal_colormaps[curvature_type],
        #                 signal_range=signal_ranges[curvature_type],
        #                 signal_lut_range=signal_lut_ranges[curvature_type])
        # map_figure.gca().set_title(filename,size=20)

        inverted_signal_map = deepcopy(signal_map)
        inverted_signal_map.confidence_map = signal_map.confidence_map[::-1]
        inverted_signal_map.signal_maps[curvature_type] = signal_map.signal_maps[curvature_type][::-1]

        # map_figure.add_subplot(3,len(time_reference_filenames)+1,2*(len(time_reference_filenames)+1)+1)
        # plot_signal_map(inverted_signal_map,curvature_type,map_figure,
        #                 colormap=signal_colormaps[curvature_type],
        #                 signal_range=signal_ranges[curvature_type],
        #                 signal_lut_range=signal_lut_ranges[curvature_type])

        rotation_thetas = signal_map.tt[:,0] - signal_map.tt[0,0]
        dr = signal_map.rr[0,1]-signal_map.rr[0,0]
        dt = signal_map.tt[1,0]-signal_map.tt[0,0]

        area_map = signal_map.rr * dr * dt

        for i_f, reference_filename in enumerate(time_reference_filenames):
            rotation_curvature_errors[reference_filename] = []
            rotation_relative_curvature_errors[reference_filename] = []
            rotation_inverted_curvature_errors[reference_filename] = []
            rotation_inverted_relative_curvature_errors[reference_filename] = []

            rotation_overlap[reference_filename] = []
            rotation_inverted_overlap[reference_filename] = []

            # map_figure.add_subplot(3, len(time_reference_filenames) + 1, i_f + 2)
            # plot_signal_map(curvature_maps[reference_filename], curvature_type, map_figure,
            #                 colormap=signal_colormaps[curvature_type],
            #                 signal_range=signal_ranges[curvature_type],
            #                 signal_lut_range=signal_lut_ranges[curvature_type])
            # map_figure.gca().set_title(reference_filename,size=20)

        for i_r, rotation_theta in enumerate(rotation_thetas):
            rotated_map = deepcopy(signal_map)
            rotated_map.confidence_map = np.concatenate([signal_map.confidence_map[i_r:], signal_map.confidence_map[:i_r]])
            rotated_map.signal_maps[curvature_type] = np.concatenate([signal_map.signal_maps[curvature_type][i_r:], signal_map.signal_maps[curvature_type][:i_r]])

            rotated_inverted_map = deepcopy(inverted_signal_map)
            rotated_inverted_map.confidence_map = np.concatenate([inverted_signal_map.confidence_map[i_r:], inverted_signal_map.confidence_map[:i_r]])
            rotated_inverted_map.signal_maps[curvature_type] = np.concatenate([inverted_signal_map.signal_maps[curvature_type][i_r:], inverted_signal_map.signal_maps[curvature_type][:i_r]])

            for i_f, reference_filename in enumerate(time_reference_filenames):
                reference_curvature_map = curvature_maps[reference_filename].signal_maps[curvature_type]
                reference_confidence = curvature_maps[reference_filename].confidence_map

                file_rotated_density = np.minimum(rotated_map.confidence_map, reference_confidence)

                file_rotated_error_map = np.abs(rotated_map.signal_maps[curvature_type] - reference_curvature_map)
                file_rotated_relative_error_map = file_rotated_error_map / np.abs(reference_curvature_map)

                file_confidence_mask = (file_rotated_density>0.5) & (signal_map.rr<pz_radius)
                rotation_overlap[reference_filename] += [area_map[file_confidence_mask].sum()]

                file_rotated_std = (file_rotated_error_map * area_map)[file_confidence_mask].sum() / area_map[file_confidence_mask].sum()
                file_rotated_relative_std = (file_rotated_relative_error_map * area_map)[file_confidence_mask].sum() / area_map[file_confidence_mask].sum()

                rotation_curvature_errors[reference_filename] += [file_rotated_std]
                rotation_relative_curvature_errors[reference_filename] += [file_rotated_relative_std]

                file_rotated_inverted_density = np.minimum(rotated_inverted_map.confidence_map, reference_confidence)

                file_rotated_inverted_error_map = np.abs(rotated_inverted_map.signal_maps[curvature_type] - reference_curvature_map)
                file_rotated_inverted_relative_error_map = file_rotated_inverted_error_map / np.abs(reference_curvature_map)

                file_inverted_confidence_mask = (file_rotated_inverted_density>0.5) & (inverted_signal_map.rr<pz_radius)
                rotation_inverted_overlap[reference_filename] += [area_map[file_inverted_confidence_mask].sum()]

                file_rotated_inverted_std = (file_rotated_inverted_error_map * area_map)[file_inverted_confidence_mask].sum() / area_map[file_inverted_confidence_mask].sum()
                file_rotated_inverted_relative_std = (file_rotated_inverted_relative_error_map * area_map)[file_inverted_confidence_mask].sum() / area_map[file_inverted_confidence_mask].sum()

                rotation_inverted_curvature_errors[reference_filename] += [file_rotated_inverted_std]
                rotation_inverted_relative_curvature_errors[reference_filename] += [file_rotated_inverted_relative_std]

        # for i_f,reference_filename in enumerate(time_reference_filenames):

            # map_figure.add_subplot(3, len(time_reference_filenames) + 1, len(time_reference_filenames)+1 + i_f + 2)
            # map_figure.gca().plot(list(np.degrees(rotation_thetas[:-1])-360)+list(np.degrees(rotation_thetas)),
            #                       rotation_curvature_errors[reference_filename][:-1]+rotation_curvature_errors[reference_filename],
            #                       color='darkcyan',linewidth=2)
            # if i_f==0:
            #     map_figure.gca().set_ylabel("Mean curvature error",size=20)
            # else:
            #     map_figure.gca().set_yticklabels([])
            # map_figure.gca().set_xticklabels([])
            # map_figure.gca().set_xlim(-180,180)
            # if curvature_type == 'mean_curvature':
            #     map_figure.gca().set_ylim(0.005,0.015)
            # elif curvature_type == 'gaussian_curvature':
            #     map_figure.gca().set_ylim(0.0001,0.0005)
            #
            # map_figure.add_subplot(3, len(time_reference_filenames) + 1, 2*(len(time_reference_filenames)+1) + i_f + 2)
            # map_figure.gca().plot(list(np.degrees(rotation_thetas[:-1])-360)+list(np.degrees(rotation_thetas)),
            #                       rotation_inverted_curvature_errors[reference_filename][:-1]+rotation_inverted_curvature_errors[reference_filename],
            #                       color='orange',linewidth=2)
            # if i_f==0:
            #     map_figure.gca().set_ylabel("Mean curvature error",size=20)
            # else:
            #     map_figure.gca().set_yticklabels([])
            # map_figure.gca().set_xlim(-180,180)
            # if curvature_type == 'mean_curvature':
            #     map_figure.gca().set_ylim(0.005,0.015)
            # elif curvature_type == 'gaussian_curvature':
            #     map_figure.gca().set_ylim(0.0001,0.0005)
            # map_figure.gca().set_xlabel('Rotation angle ($^\circ$)', size=20)

        # map_figure.set_size_inches(10 * (len(time_reference_filenames)+1), 3*10)
        # map_figure.tight_layout()
        # map_figure.savefig(filename + "_curvature_maps.png")
    #
    #
    figure = plt.figure(0)
    figure.clf()

    for i_file, filename in enumerate(filenames):

        time_reference_filenames = [f for f in curvature_maps.keys() if int(f[-2:]) == int(filename[-2:])]

        for i_f, reference_filename in enumerate(time_reference_filenames):
            figure.gca().plot(list(np.degrees(rotation_thetas[:-1]) - 360) + list(np.degrees(rotation_thetas)),
                              rotation_curvature_errors[reference_filename][:-1] + rotation_curvature_errors[reference_filename],
                              color='darkcyan', alpha=0.1)
            figure.gca().plot(list(np.degrees(rotation_thetas[:-1]) - 360) + list(np.degrees(rotation_thetas)),
                              rotation_inverted_curvature_errors[reference_filename][:-1] + rotation_inverted_curvature_errors[reference_filename],
                              color='orange', alpha=0.1)

    average_error = np.mean([rotation_curvature_errors[f] for f in curvature_maps.keys()],axis=0)
    figure.gca().plot(list(np.degrees(rotation_thetas[:-1])-360)+list(np.degrees(rotation_thetas)),
                      list(average_error[:-1])+list(average_error),
                      color='darkcyan',linewidth=2,alpha=0.5)
    average_inverted_error = np.mean([rotation_inverted_curvature_errors[f] for f in curvature_maps.keys()],axis=0)
    figure.gca().plot(list(np.degrees(rotation_thetas[:-1])-360)+list(np.degrees(rotation_thetas)),
                      list(average_inverted_error[:-1])+list(average_inverted_error),
                      color='orange',linewidth=2,alpha=0.5)

    if np.min(average_inverted_error) < np.min(average_error):
        orientation = -1
        theta_min = rotation_thetas[np.argmin(average_inverted_error)]
        error_min = np.min(average_inverted_error)
    else:
        orientation = 1
        theta_min = rotation_thetas[np.argmin(average_error)]
        error_min = np.min(average_error)

    theta_matrix = np.array([[np.cos(theta_min),-np.sin(theta_min)],[np.sin(theta_min),np.cos(theta_min)]])
    orientation_matrix = np.array([[1,0],[0,orientation]])

    theta_min = (np.degrees(theta_min)+180)%360 - 180

    logging.info("  --> Orientation = "+str(orientation)+", Rotation angle = "+str(np.round(theta_min,2))+"°")

    figure.gca().scatter([theta_min],[error_min],color='orange' if orientation==-1 else 'darkcyan',marker='v',s=100)
    figure.gca().set_title("Orientation = "+str(orientation)+", Rotation angle = "+str(np.round(theta_min,2))+"$^\circ$")

    figure.gca().set_xlim(-180,180)
    if curvature_type == 'mean_curvature':
        figure.gca().set_ylim(0.005,0.02)
    elif curvature_type == 'gaussian_curvature':
        figure.gca().set_ylim(0.0001,0.0005)
    figure.gca().set_xlabel('Rotation angle ($^\circ$)', size=20)
    figure.gca().set_ylabel("Mean curvature error",size=20)

    figure.set_size_inches(10,5)
    figure.tight_layout()
    figure.savefig(sequence_name + "_curvature_rotation_error.png")

    # figure = plt.figure(2)
    # figure.clf()
    #
    # for i_f, reference_filename in enumerate(time_reference_filenames):
    #     figure.gca().plot(list(np.degrees(rotation_thetas[:-1]) - 360) + list(np.degrees(rotation_thetas)),
    #                       rotation_overlap[reference_filename][:-1] + rotation_overlap[reference_filename],
    #                       color='darkcyan', alpha=0.1)
    #     figure.gca().plot(list(np.degrees(rotation_thetas[:-1]) - 360) + list(np.degrees(rotation_thetas)),
    #                       rotation_inverted_overlap[reference_filename][:-1] + rotation_inverted_overlap[reference_filename],
    #                       color='orange', alpha=0.1)

    for i_file, filename in enumerate(filenames):
        logging.info("  --> Applying alignment transform on " + filename)

        file_data = sequence_data[filename]

        X = file_data['centered_x'].values
        Y = file_data['centered_y'].values
        Z = file_data['centered_z'].values

        centered_positions = np.transpose([X, Y, Z])
        theta_positions = np.einsum('...ij,...i->...j', theta_matrix, np.einsum('...ij,...i->...j', orientation_matrix, centered_positions[:, :2]))

        sequence_data[filename]['aligned_x'] = theta_positions[:,0]
        sequence_data[filename]['aligned_y'] = theta_positions[:,1]
        sequence_data[filename]['aligned_z'] = centered_positions[:,2]

        sequence_data[filename]['radial_distance'] = np.linalg.norm(sequence_data[filename][['aligned_x','aligned_y']].values, axis=1)
        cosines = sequence_data[filename]['aligned_x'] / sequence_data[filename]['radial_distance']
        sequence_data[filename]['aligned_theta'] = np.degrees(np.sign(sequence_data[filename]['aligned_y']) * np.arccos(cosines))


if __name__ == "__main__":
    aligned_sequence_database = load_aligned_sam_database()

    reference_angle_gaps = {}

    for reference_sequence_name in list(aligned_sequence_database.keys()):
        print("--> Aligning {}".format(reference_sequence_name))
        centered_data = aligned_sequence_database[reference_sequence_name].copy()

        for filename in centered_data.keys():
            for dim in ['x', 'y', 'z']:
                centered_data[filename]['centered_'+dim] = centered_data[filename]['aligned_'+dim]
            centered_data[filename]['radial_distance'] = np.linalg.norm(centered_data[filename][['centered_x', 'centered_y']].values, axis=1)
            cosines = centered_data[filename]['centered_x'] / centered_data[filename]['radial_distance']
            centered_data[filename]['centered_theta'] = np.degrees(np.sign(centered_data[filename]['centered_y']) * np.arccos(cosines))

        excluded_database = {s:d for s,d in aligned_sequence_database.items() if s!=reference_sequence_name}

        centered_meristem_curvature_map_alignment(centered_data, excluded_database, map_resolution=1, pz_radius=70)

        centered_angles = centered_data[list(centered_data.keys())[0]]['centered_theta'].values
        aligned_angles = centered_data[list(centered_data.keys())[0]]['aligned_theta'].values

        angle_gap = np.mean((centered_angles-aligned_angles+180)%360 - 180)
        reference_angle_gaps[reference_sequence_name] = angle_gap

    figure = plt.figure(0)
    figure.clf()

    figure.gca().hist(list(reference_angle_gaps.values()),
                      bins=np.linspace(-180, 180, 31),
                      color='g',
                      ec='k')

    figure.gca().set_xlabel("Aligned angle error ($^\circ$)",size=20)
    figure.gca().set_ylabel("Number of SAMs",size=20)

    figure.show()