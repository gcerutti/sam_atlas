import os
import logging

import numpy as np
from scipy import ndimage as nd
import pandas as pd

#import matplotlib.pyplot as plt
#import matplotlib.patches as patch

from tissue_nukem_3d.epidermal_maps import compute_local_2d_signal, nuclei_density_function
from tissue_nukem_3d.signal_map import SignalMap
#from tissue_nukem_3d.signal_map_visualization import plot_signal_map
from tissue_nukem_3d.signal_map_analysis import signal_map_regions
#from tissue_nukem_3d.utils.signal_luts import signal_colormaps, signal_ranges, signal_lut_ranges

from cellcomplex.utils import array_dict

from sam_atlas.utils import golden_angle

golden_angle = np.degrees(golden_angle)


def extract_clv3_circle(signal_data, position_name='center', clv3_threshold_range=np.linspace(1.2, 1.8, 7), cell_radius=5, density_k=0.33, loglevel=0):
    if not "Normalized_CLV3" in signal_data.columns:
        if not "CLV3" in signal_data.columns:
            raise KeyError("CLV3 signal is not defined! Unable to estimate CZ position!")
        else:
            signal_data['Normalized_CLV3'] = signal_data['CLV3'] / signal_data[signal_data['layer'] == 1]['CLV3'].mean()

    signal_map = SignalMap(signal_data, extent=200, position_name=position_name, resolution=2, polar=False, radius=cell_radius, density_k=density_k)

    # clv3_map = signal_map.signal_map("Normalized_CLV3")

    centers = []
    radii = []
    for clv3_threshold in clv3_threshold_range:

        clv3_regions = signal_map_regions(signal_map, 'Normalized_CLV3', threshold=clv3_threshold)
        if len(clv3_regions) > 0:
            clv3_region = clv3_regions.iloc[np.argmax(clv3_regions['area'])]
            clv3_radius = np.sqrt(clv3_region['area'] / np.pi)
            clv3_center = clv3_region[['center_x', 'center_y']].values
            centers += [clv3_center]
            radii += [clv3_radius]

        # clv3_regions = nd.label((clv3_map>clv3_threshold).astype(int))[0]
        # components = np.unique(clv3_regions)[1:]
        # component_centers = np.transpose([nd.sum(xx,clv3_regions,index=components),nd.sum(yy,clv3_regions,index=components)])/nd.sum(np.ones_like(xx),clv3_regions,index=components)[:,np.newaxis]
        # component_areas = np.array([(clv3_regions==c).sum() * np.prod(resolution) for c in components])

        # if len(component_centers)>0:
        #     clv3_center = component_centers[np.argmax(component_areas)]
        #     clv3_area = np.max(component_areas)
        #     clv3_radius = np.sqrt(clv3_area/np.pi)

        #     centers += [clv3_center]
        #     radii += [clv3_radius]

    clv3_center = np.mean(centers, axis=0)
    clv3_radius = np.mean(radii)

    return clv3_center, clv3_radius


def optimize_vertical_axis(positions, angle_max=0.2, angle_resolution=0.01, r_max=80, loglevel=0):
    """
    """

    angles = np.linspace(-angle_max, angle_max, int(np.ceil(2 * (angle_max / angle_resolution) + 1)))

    psis, phis = np.meshgrid(angles, angles)

    rotation_mse = []

    for dome_phi in angles:
        phi_mse = []
        for dome_psi in angles:
            rotation_matrix_psi = np.array([[1, 0, 0], [0, np.cos(dome_psi), -np.sin(dome_psi)], [0, np.sin(dome_psi), np.cos(dome_psi)]])
            rotation_matrix_phi = np.array([[np.cos(dome_phi), 0, -np.sin(dome_phi)], [0, 1, 0], [np.sin(dome_phi), 0, np.cos(dome_phi)]])
            rotated_positions = np.einsum('...ij,...j->...i', rotation_matrix_psi, positions)
            rotated_positions = np.einsum('...ij,...j->...i', rotation_matrix_phi, rotated_positions)

            rotated_r = np.linalg.norm(rotated_positions[:, :2], axis=1)
            rotated_z = rotated_positions[:, 2]
            r_weights = np.exp(-np.power(rotated_r, 2) / np.power(20, 2))
            p = np.polyfit(rotated_r, rotated_z, deg=2, w=r_weights)

            r = np.linspace(0, r_max, int(r_max + 1))
            mse = (r_weights * np.power(rotated_z - np.polyval(p, rotated_r), 2)).sum() / (r_weights.sum())
            phi_mse += [mse]
        rotation_mse += [phi_mse]

    optimal_rotation = np.where(rotation_mse == np.array(rotation_mse).min())
    optimal_phi = (phis[optimal_rotation]).mean()
    optimal_psi = (psis[optimal_rotation]).mean()

    logging.info("".join(["  " for l in range(loglevel)]) + "--> Optimal angles : (" + str(optimal_phi) + ", " + str(optimal_psi) + ")")

    rotation_matrix_psi = np.array([[1, 0, 0], [0, np.cos(optimal_psi), -np.sin(optimal_psi)], [0, np.sin(optimal_psi), np.cos(optimal_psi)]])
    rotation_matrix_phi = np.array([[np.cos(optimal_phi), 0, -np.sin(optimal_phi)], [0, 1, 0], [np.sin(optimal_phi), 0, np.cos(optimal_phi)]])
    rotated_positions = np.einsum('...ij,...j->...i', rotation_matrix_psi, positions)
    rotated_positions = np.einsum('...ij,...j->...i', rotation_matrix_phi, rotated_positions)

    return rotated_positions, np.dot(rotation_matrix_phi, rotation_matrix_psi)


def center_sam_sequence(sequence_data, sequence_rigid_transforms, sequence_sam_centers=None, sam_orientation=1, r_max=120, cell_radius=5., density_k=0.25, microscope_orientation=-1, loglevel=0):

    filenames = np.sort(list(sequence_data.keys()))
    sequence_name = filenames[0][:-4]

    for i_file, filename in enumerate(filenames):
        sequence_data[filename] = sequence_data[filename][sequence_data[filename]['layer'] == 1]

        file_data = sequence_data[filename]

        X = file_data['center_x'].values
        Y = file_data['center_y'].values
        Z = file_data['center_z'].values

        if i_file == 0:
            file_data['registered_x'] = X
            file_data['registered_y'] = Y
            file_data['registered_z'] = Z

            # file_data['sequence_registered_x'] = X
            # file_data['sequence_registered_y'] = Y
            # file_data['sequence_registered_z'] = Z

    previous_transform = np.diag(np.ones(4))

    if sequence_sam_centers is not None:
        sequence_registered_sam_centers = {filenames[0]:microscope_orientation*sequence_sam_centers[filenames[0]]}

    for i_file, (reference_filename, floating_filename) in enumerate(zip(filenames[:-1], filenames[1:])):
        logging.info("".join(["  " for l in range(loglevel)]) + "--> Computing sequence registered points " + reference_filename + " --> " + floating_filename)

        rigid_matrix = sequence_rigid_transforms[(reference_filename, floating_filename)]
        invert_rigid_matrix = sequence_rigid_transforms[(floating_filename, reference_filename)]

        reference_data = sequence_data[reference_filename]
        floating_data = sequence_data[floating_filename]

        X = reference_data['center_x'].values
        Y = reference_data['center_y'].values
        Z = reference_data['center_z'].values

        reference_points = np.transpose([X, Y, Z])

        cell_barycenters = array_dict(np.transpose([X, Y, Z]), reference_data.index.values)

        X = floating_data['center_x'].values
        Y = floating_data['center_y'].values
        Z = floating_data['center_z'].values

        cell_barycenters = array_dict(np.transpose([X, Y, Z]), floating_data.index.values)

        homogeneous_points = np.concatenate([microscope_orientation * np.transpose([X, Y, Z]), np.ones((len(X), 1))], axis=1)
        #homogeneous_points = np.concatenate([np.transpose([X, Y, Z]), np.ones((len(X), 1))], axis=1)
        registered_points = np.einsum("...ij,...j->...i", invert_rigid_matrix, homogeneous_points)
        #registered_points = np.einsum("...ij,...j->...i", rigid_matrix, homogeneous_points)
        registered_points = microscope_orientation * registered_points[:, :3]
        #registered_points = registered_points[:, :3]

        if sequence_sam_centers is not None:
            floating_center = sequence_sam_centers[floating_filename]
            #homogeneous_center = np.array([list(microscope_orientation * floating_center) + [0., 1]])
            homogeneous_center = np.array([list(floating_center) + [0., 1]])
            registered_center = np.einsum("...ij,...j->...i", invert_rigid_matrix, homogeneous_center)[0]
            sequence_registered_sam_centers[floating_filename] = microscope_orientation * registered_center[:2]
            #sequence_registered_sam_centers[floating_filename] = registered_center[:2]

        # previous_transform = np.dot(invert_rigid_matrix, previous_transform)
        # #previous_transform = np.dot(rigid_matrix, previous_transform)
        # sequence_registered_points = np.einsum("...ij,...j->...i", previous_transform, homogeneous_points)
        # sequence_registered_points = microscope_orientation * sequence_registered_points[:, :3]

        floating_data['registered_x'] = registered_points[:, 0]
        floating_data['registered_y'] = registered_points[:, 1]
        floating_data['registered_z'] = registered_points[:, 2]

        # floating_data['sequence_registered_x'] = sequence_registered_points[:, 0]
        # floating_data['sequence_registered_y'] = sequence_registered_points[:, 1]
        # floating_data['sequence_registered_z'] = sequence_registered_points[:, 2]

    if sequence_sam_centers is not None:
        print(sequence_registered_sam_centers)

    # registration_filenames = [f for f in filenames]
    registration_filenames = [f for f in filenames if (not 't04' in f) and (int(f[-2:]) <= 10)]
    logging.info("".join(["  " for l in range(loglevel)]) + "--> Aligning SAM sequence using " + str([f[-3:] for f in registration_filenames]))

    logging.info("".join(["  " for l in range(loglevel)]) + "  --> Computing optimal vertical orientation")

    registration_data = pd.concat([sequence_data[f] for f in registration_filenames])
    X = np.concatenate([sequence_data[f]['registered_x'].values for f in registration_filenames])
    Y = np.concatenate([sequence_data[f]['registered_y'].values for f in registration_filenames])
    Z = np.concatenate([sequence_data[f]['registered_z'].values for f in registration_filenames])

    if sequence_sam_centers is None:
        clv3_center, clv3_radius = extract_clv3_circle(registration_data, position_name='registered', cell_radius=cell_radius, density_k=density_k)
        logging.info("".join(["  " for l in range(loglevel)]) + "    --> CZ Circle : " + str(clv3_center) + " (" + str(clv3_radius) + ")")
    else:
        clv3_center = np.mean([sequence_registered_sam_centers[filename] for filename in filenames],axis=0)

    center_altitude = compute_local_2d_signal(np.transpose([X, Y]), clv3_center, Z)[0]
    print(np.array(list(clv3_center) + [center_altitude]))
    centered_positions = np.transpose([X, Y, Z]) - np.array(list(clv3_center) + [center_altitude])

    rotated_positions, rotation_matrix = optimize_vertical_axis(centered_positions, r_max=r_max, loglevel=loglevel + 2)
    #
    # figure = plt.figure(1)
    # figure.clf()
    # figure.patch.set_facecolor('w')
    #
    # figure.add_subplot(2, 1, 1)
    # figure.gca().scatter(np.linalg.norm(centered_positions[:, :2], axis=1), centered_positions[:, 2], linewidth=0, alpha=0.6)
    # figure.gca().set_xlim(0, 120)
    # figure.gca().set_ylim(-50, 10)
    #
    # figure.add_subplot(2, 1, 2)
    # figure.gca().scatter(np.linalg.norm(rotated_positions[:, :2], axis=1), rotated_positions[:, 2], linewidth=0, alpha=0.6)
    # figure.gca().set_xlim(0, 120)
    # figure.gca().set_ylim(-50, 10)
    #
    # figure.set_size_inches(10, 10)
    # figure.savefig(sequence_name + "_vertical_axis_optimization.png")

    for i_file, filename in enumerate(filenames):
        logging.info("".join(["  " for l in range(loglevel)]) + "  --> Applying centering transform on " + filename)

        file_data = sequence_data[filename]

        X = file_data['registered_x'].values
        Y = file_data['registered_y'].values
        Z = file_data['registered_z'].values

        centered_positions = np.transpose([X, Y, Z]) - np.array(list(clv3_center) + [center_altitude])
        rotated_positions = np.einsum('...ij,...j->...i', rotation_matrix, centered_positions)

        sequence_data[filename]['centered_x'] = rotated_positions[:, 0]
        sequence_data[filename]['centered_y'] = rotated_positions[:, 1]
        sequence_data[filename]['centered_z'] = rotated_positions[:, 2]


    # figure = plt.figure(2)
    # figure.clf()
    # figure.patch.set_facecolor('w')
    #
    # for i_file, filename in enumerate(filenames):
    #     figure.add_subplot(len(filenames),2,2*i_file+1)
    #     file_data = sequence_data[filename]
    #
    #     signal_map = SignalMap(file_data, extent=200, position_name='center', resolution=2, polar=False, radius=cell_radius, density_k=density_k)
    #     signal_map.compute_signal_map('CLV3')
    #     plot_signal_map(signal_map,'CLV3', figure,
    #                     colormap=signal_colormaps['CLV3'],
    #                     signal_range=signal_ranges['CLV3'],
    #                     signal_lut_range=signal_lut_ranges['CLV3'])
    #
    #     figure.add_subplot(len(filenames),2,2*i_file+2)
    #
    #     signal_map = SignalMap(file_data, extent=200, position_name='centered', resolution=2, polar=False, radius=cell_radius, density_k=density_k)
    #     signal_map.compute_signal_map('CLV3')
    #     plot_signal_map(signal_map, 'CLV3', figure,
    #                     colormap=signal_colormaps['CLV3'],
    #                     signal_range=signal_ranges['CLV3'],
    #                     signal_lut_range=signal_lut_ranges['CLV3'])
    #
    # figure.set_size_inches(20,10*len(filenames))
    # figure.savefig(sequence_name + "_CLV3_centering.png")
