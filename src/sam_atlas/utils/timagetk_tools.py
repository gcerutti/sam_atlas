import logging
from time import time as current_time

import numpy as np
import scipy.ndimage as nd

from timagetk.components import SpatialImage
from timagetk.plugins import h_transform, region_labeling, segmentation


def auto_seeded_watershed(img_dict, membrane_channel='PI', gaussian_sigma=0.75, h_min=200, segmentation_gaussian_sigma=0.5, volume_threshold=None, background_label=1):
    """

    Parameters
    ----------
    img_dict
    membrane_channel
    gaussian_sigma
    h_min
    segmentation_gaussian_sigma
    volume_threshold
    background_label

    Returns
    -------

    """

    img = img_dict[membrane_channel]
    voxelsize = np.array(img.voxelsize)

    step_start_time = current_time()
    smooth_image = nd.gaussian_filter(img, sigma=gaussian_sigma / voxelsize).astype(img.dtype)
    smooth_img = SpatialImage(smooth_image, voxelsize=voxelsize)
    logging.info("    --> Smoothing image for seed detection [" + str(current_time() - step_start_time) + "s]")

    step_start_time = current_time()
    ext_img = h_transform(smooth_img, h=h_min, method='min')
    logging.info("    --> Computing H-transform [" + str(current_time() - step_start_time) + "s]")

    step_start_time = current_time()
    seed_img = region_labeling(ext_img, low_threshold=1, high_threshold=h_min, method='connected_components')
    logging.info("    --> Extracting seed image [" + str(current_time() - step_start_time) + "s]")

    step_start_time = current_time()
    seg_smooth_image = nd.gaussian_filter(img, sigma=segmentation_gaussian_sigma / voxelsize).astype(img.dtype)
    seg_smooth_img = SpatialImage(seg_smooth_image, voxelsize=voxelsize)
    logging.info("    --> Smoothing image for segmentation [" + str(current_time() - step_start_time) + "s]")

    step_start_time = current_time()
    seg_img = segmentation(seg_smooth_img, seed_img, control='first', method='seeded_watershed')
    logging.info("    --> Performing watershed segmentation [" + str(current_time() - step_start_time) + "s]")

    if volume_threshold is not None:
        seg_volumes = dict(zip(np.arange(seg_img.max()) + 1,
                               nd.sum(np.prod(voxelsize) * np.ones_like(seg_img),
                                      seg_img,
                                      index=np.arange(seg_img.max()) + 1)))

        labels_to_remove = np.array(list(seg_volumes.keys()))[np.array(list(seg_volumes.values())) > volume_threshold]
        logging.info("    --> Removing too large labels :" + str(labels_to_remove))
        for l in labels_to_remove:
            seg_img[seg_img == l] = background_label

    return seg_img
