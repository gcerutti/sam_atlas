# {# pkglts, pysetup.kwds
# format setup arguments

from setuptools import setup, find_packages


short_descr = "Tools and resources for tissue identification and spatial registration of Shoot Apical Meristems"
readme = open('README.rst').read()
history = open('HISTORY.rst').read()

# find packages
pkgs = find_packages('src')



setup_kwds = dict(
    name='sam_atlas',
    version="0.1.0",
    description=short_descr,
    long_description=readme + '\n\n' + history,
    author="Guillaume Cerutti",
    author_email="guillaume.cerutti@ens-lyon.fr",
    url='https://gitlab.inria.fr/gcerutti/sam_atlas',
    license='cecill-c',
    zip_safe=False,

    packages=pkgs,
    
    package_dir={'': 'src'},
    setup_requires=[
        ],
    install_requires=[
        ],
    tests_require=[
        ],
    entry_points={},
    keywords='',
    )
# #}
# change setup_kwds below before the next pkglts tag

# do not change things below
# {# pkglts, pysetup.call
setup(**setup_kwds)
# #}
